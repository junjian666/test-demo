# 通过%符号占位已经很方便了，还能进行精度控制，但是有更效率的解决方式
# f"内容{变量}"，这个快速格式化不限数据类型，也不做精度控制，原本是什么样，就输出什么样
# f就是format

name = "传播"
set_up_year = 2006
stock_price = 19.99
print(f"我是{name}，我成立于：{set_up_year}年，我今天的股价是：{stock_price}")