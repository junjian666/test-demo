# 运算符
# // 取整数， 9//2 等于 4
# % 取余， 返回除法的余数，减去整除的数，余下的数；9除以2，2乘以4等于8,1减去9等于1，有一个1除不开，所以余1
# ** 指数，a**b的20次方，输出结果1000000000000000000000000000000；2的2次方 等于4

# 算术运算符
print("1 + 1 =", 1 + 1)
print("2 - 1 =", 2 - 1)
print("3 * 3 =", 3 * 3)
print("4 / 2 =", 4 / 2)
print("11 // 2 =", 11 // 2)
print("9 % 2 =", 9 % 2)
print("2 ** 2 =", 2 ** 2)

# 赋值运算符， =就是赋值运算符，把等号右边的结果赋给左边的变量，
num = 1 + 2 * 3

"""
复合赋值运算符，
c += a 等于 c = c+a
c -= a 等于 c = c-a
c *= a 等于 c = c*a
c /= a 等于 c = c/a
c %= a 等于 c = c%a
c **= a 等于 c = c**a
c //= a 等于 c = c//a
"""

num = 1
num += 1 # 也就是num = num+1
print("num += 1 :", num)
num -=1
print("num -=1 :", num)
num *=4
print("num *=4 :", num)
num /=2
print("num /-2 :", num)

num = 3 # 重新使num =3
num %=2
print("num %= 2 :", num)
num **=2
print("num **= 2 :",num)

num = 9 # 重新使num =9
num //=2
print("num //=2 : ", num)