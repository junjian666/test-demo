# 掌握布尔类型用于表示真和假，True表示真，记作1，False表示假，记作0；不仅可以自行定义，同时也可以通过比较运算符得来，
# 变量名称 = 布尔类型字面量
# 掌握比较运算符用于计算真和假，比较运算符进行比较运算同样可以得到布尔类型的结果。
# 比较运算符
# == 判断内容是否相等
# != 判断内容是否不相等
# > 判断左侧内容是否大于右侧
# < 判断左侧内容是否小于右侧
# >= 判断左侧内容是否大于等于右侧
# <= 判断左侧内容是否小于等于右侧

result = 10 > 5
print(f"10 > 5的结果是: {result}，类型是{type(result)}")

result = "abc" == "aaa"
print(f"字符串abc是否和aaa相等，结果是：{result}，类型是{type(result)}")

# 定义变量存储布尔类型的数据
# 布尔类型只有两个字面量，True和False
bool_1 = True #定义变量，在字面量前面加上赋值语句就可以了
bool_2 = False
print(f"bool_1变量的内容是：{bool_1}，类型是:{type(bool_1)}")
print(f"bool_1变量的内容是：{bool_2}，类型是:{type(bool_2)}")

# 演示进行内容的相等比较
num1 = 10
num2 = 10
print(f"10 == 10的结果是：{num1 == num2 }")

num1 = 10
num2 = 15
print(f"10 != 15的结果是：{num1 != num2}")

name1 = "abc"
name2 = "aaa"
print(f"abc == aaa的结果是：{name1 == name2 }")

num1 = 10
num2 = 5
print(f"10 > 5的结果是：{num1 > num2}")
print(f"10 < 5的结果是：{num1 < num2}")

num1 = 10
num2 = 11
print(f"10 >= 11的结果是：{num1 >= num2}")
print(f"10 <+ 11的结果是：{num1 <= num2}")
