# 掌握input语句的使用方式
# print 数据输出
# input 数据输入，默认接收的类型是字符串，如果想得到其他数据类型，需要转换

print("请告诉我你是谁？")
name = input() # 赋值运算符，把右边的结果富裕左边的变量
print("我知道了，你是： %s" % name) # 在返回栏里输入我是黑马，回车，就会被上面的input接收

# 比上面省略一句
name = input("请告诉我你是谁？")
print("我知道了，你是 %s" % name)

# 输入数字类型
num = input("请告诉我你的银行卡密码：")
print("你的银行卡密码的类型是：",type(num))
# 上面的数据类型是字符串，所以需要数据类型转换，int(num)
num = input("请告诉我你的银行卡密码：")
num = int(num)
print("你的银行卡密码的类型是：",type(num))

user_name = input("用户名称")
user_type = input("用户类型")
print("您好：%s 您是尊贵的%s用户，欢迎您的光临" % (user_name,user_type))