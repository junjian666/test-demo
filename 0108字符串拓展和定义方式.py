"""
 字符串在python中有多重定义形式：
 单引号: name = '黑马'
 双引号: name = "黑马"
 三引号：name =  三引号支持换行，使用变量接收他，他就是字符串，如果不使用变量接收他，就可以作为多行注释

"""

name = '黑马'
print(type(name))
name = "黑马"
print(type(name))
name = """
黑
马
"""
print(type(name))

# 想定义字符串本身，如何写
# 单引号定义法，可以内含双引号
name = '"黑马"'
print(name)
# 双引号定义法，可以内含单引号
name = "'黑马'"
print(name)
# 引号的嵌套，也可以使用转移字符 \ 来将引号接触效用，编程普通字符串, \让单双引号不再有意义，编程普通的符号
name = "\"黑马\""
print(name)
name = '\'黑马\''
print(name)
