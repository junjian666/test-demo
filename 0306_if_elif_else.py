# 该语句的作用是可以完成多个条件的判断
print("欢迎来到黑马动物园")
height = int(input("请输入你的身高(cm) : "))
VIP_level = int(input("请输入你的VIP级别(1~5) : "))
day = int(input("请告诉我今天是几号："))
if height < 120 :
    print("您的身高小于120CM，可以免费游玩。")
elif VIP_level > 3 :
    print("您的VIP级别大于3，可以免费游玩。")
elif day == 1 :
    print("今天是1号免费日，可以免费")
else:
    print("不好意思，所有条件都不满足，需要购票10元。")
print("祝您游玩愉快")

# 判断是互斥且有顺序的，满足1，将不会理会2和3
# 满足2，将不会理会3,；1,2,3均不满足，进入else
# else也可以省略不写，效果等同3个独立的if判断
# 如下所示，可以将input输入语句直接写入判断条件中，节省代码量，满足1的情况下，2和3不会再出现
if int(input("请输入你的身高(cm) : ")) < 120 :
    print("您的身高小于120CM，可以免费游玩。")
elif int(input("请输入你的VIP级别(1~5) : ")) > 3 :
    print("您的VIP级别大于3，可以免费游玩。")
elif int(input("请告诉我今天是几号：")) == 1:
    print("今天是1号免费日，可以免费")
else:
    print("不好意思，所有条件都不满足，需要购票10元。")
print("祝您游玩愉快")

num = 5
if int(input("请输入第一次猜想的数字：")) == num :
    print("恭喜你，第一次就猜对了")
elif int(input("猜错了，再猜一次：")) == num :
    print("恭喜你，第二次就猜对了")
elif int(input("猜错了，再猜一次：")) == num :
    print("恭喜你，最后一次猜对了")
else :
    print("sorry，全部猜错啦")
