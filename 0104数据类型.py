"""
type()语句，给返回值

我们可以通过type()语句来得到数据的类型
通过该语句查看的是变量存储的数据的类型，因为变量无类型，但是他存储的数据有
字符串变量：变量里存储的是字符串
万物只要套上双引号，都是字符串
"""

# 使用print直接输出类型信息
print(type(666))
print(type("黑马"))
print(type(13.14))

# 使用变量存储type()语句的结果
string_type = type("黑马")
int_type = type(666)
float_type = type(13.14)
print(string_type)
print(int_type)
print(float_type)

# 查看变量中存储的数据类型
name = "黑马"
name_type = type(name)
print(name_type)
