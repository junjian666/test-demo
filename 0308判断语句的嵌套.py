# if语句嵌套在另一个if语句里，当外层if满足条件时，才会执行内层if判断
# 当外层不满足时，直接执行外层esle

if int(input("你的身高是多少：")) > 120:
    print("身高超出限制，不可以免费")
    print("但是，如果vip级别大于3，可以免费")
    if int(input("你的vip级别是多少：")) > 3 :
        print("恭喜你，vip级别达标，可以免费")
    else:
        print("sorry, 你需要买票10元")
else :
    print("欢迎小朋友，免费游玩")

# 必须是大于等于18岁，小于30岁的成年人
# 同时入职时间必须满足大于2年，或者级别大于3才可以领取
age = 22
year = 1
level = 5
if age >= 18 :
    print("你是成年人")
    if age < 30 :
        print("你的年龄达标了")
        if year > 2:
            print("恭喜你，年龄和入职时间都达标，可以领取礼物")
        elif level > 3 :
            print("恭喜你，年龄和级别达标，可以领取礼物")
        else :
            print("不好意思，尽管年龄达标，但是入职时间和及级别都不达标")
    else :
        print("不好意思，年龄太大了")
else:
    print("不好意思，小朋友不可以领取")