# 字符串转数字，数字转字符串
# 从文件中读取的数字，默认是字符串，需要把它转换成数字类型
# 后续学习的input语句，默认结果是字符串，若需要数字也得转换
# 将x转换为一个整数，int(x); 将x转换为一个浮点数，float(x); 将x转换为一个字符串，str(x)


# 将数字类型转换成字符串,变量名字 = 变量值
num_str=str(11)
print(type(num_str), num_str)

float_str = str(13)
print(type(float_str), float_str)
# 将字符串转换成数字
num = int("11")
print(type(num), num)

num2 = float("11.13")
print(type(num2), num2)

# 错误示例，想把字符串转换成数字，必须要求字符串内的内容都是数字
# num3 = int("黑马")
# print(type(num3), num3)

# 整数转浮点数
float_num = float(11)
print(type(float_num), float_num)

# 浮点数转整数,会丢失精度
int_num = int(11.14)
print(type(int_num), int_num)