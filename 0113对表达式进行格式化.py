# 表达式：一条具有明确执行结果的代码语句
# 如：1+1  %*2   或者  name = "张三"  age = 11 + 11
# 无需使用变量进行数据存储的时候，可以直接格式化表达式，以简单代码

print("1 * 1 的结果是： %d" % (1*1)) # 1 * 1就是表达式
print(f"1 * 2 的结果是：{1 * 2}") # 1 * 2 就是表达式
print("字符串在Python中的类型名是：%s" % type("字符串")) # type("字符串")是表达式

# 格式化表达式：f"(表达式)"  "%s %d %f" % (表达式，表达式，表达式)

name = '传播'
stock_price = 19.99
stock_code = '003032' # 数字不能以0开头，所以要用字符串
stock_price_daily_growth_factor = 1.2
growth_days = 7
print(f"公司：{name}，股票代码：{stock_code}，当前股价：{stock_price}")
print("每日增长系数是 %.1f，经过%d天增长后，股价达到了: %.2f"  % (stock_price_daily_growth_factor, growth_days, (19.99*1.2**7)))
# print("数字11宽度限制5，结果是：%5d" % num1)