# if 要判断的条件:
#     当条件成立时，要做什么(前面需要四个空格缩进)
#

age = 30
if age >= 18:
    print("我已经成年了")

age = 18
if age >= 18:
    print("我已经成年了")
    print("即将步入大学生活")

print("时间过的真快")

age = 10
if age >= 18:
    print("我已经成年了")
    print("即将步入大学生活")

print("欢迎来到黑马儿童游乐场，儿童免费，成人收费")
age = input("请输入你的年龄:")
age = int(age) # age = int(input("请输入你的年龄:"))
if age >= 18 :
    print("您已成年，游玩需要补票10元")
print("祝您游玩愉快")

