# print("Hello")
# print("World")

# 不换行
# print("Hello",end='')
# print("World",end='')

# 特殊符号 \t ,效果等同于在键盘上按下tab键，可以让我们的多行字符串对齐
print("Hello\tWorld")
print("IThei\tma")

# 打印九九乘法表
# 定义外层循环的控制变量，控制行
i = 1
while i <= 9:
    # 定义内层循环的控制变量，控制列
    j = 1
    while j <= i:
        # 内层循环的print语句，不要换行，用\t制表符进行对齐
        print(f"{j} * {i} = {j * i}\t", end='')
        j += 1
    i += 1
    print() #print空内容，就是输出一个换行

#解读：当i=1的时候开始输出，j也等于1，那么1乘以1等于1，不换行，j加1，不满足小于等于i，那么内层循环结束
# i = 2，j等于1，那么1乘以2等于2，不换行，j加1等于，满足小于等于i，那么2乘以2等4，不换行，j再加1等于3，不满足小于等于i，那么内层结束。
# i= 3，
